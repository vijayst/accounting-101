//
//  Transaction.h
//  Accounting 101
//
//  Created by Vijay Thirugnanam on 06/01/14.
//  Copyright (c) 2014 Vijay Thirugnanam. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BalanceSheet.h"

@interface Transaction : NSObject
@property int num;
@property float debit;
@property int type;
@property NSDate *date;
@property int quantity;
@property float rate;

+(void)reset;
+(Transaction *)getFirst;
+(Transaction *)getLatest;

-(float)getDebit:(int)ptype;
-(float)getDebit:(int)ptype from:(NSDate *)from to:(NSDate *)to;
-(int)getQuantity:(int)ptype;

-(float)credit:(int)type;
-(float)debit:(int)type;
-(float)credit:(int)type date:(NSDate *)date;
-(float)debit:(int)type date:(NSDate *)date;
-(float)credit:(int)type quantity:(int)quantity;
-(float)debit:(int)type quantity:(int)quantity;
-(float)credit:(int)type quantity:(int)quantity date:(NSDate *)date;
-(float)debit:(int)type quantity:(int)quantity date:(NSDate *)date;
-(float)getRate:(int)type;
@end
