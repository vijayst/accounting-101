//
//  SetGameView.h
//  Accounting 101
//
//  Created by Vijay Thirugnanam on 05/01/14.
//  Copyright (c) 2014 Vijay Thirugnanam. All rights reserved.
//

#import "PopupView.h"
#import "Game.h"


@protocol OperationSetDelegate
@optional
-(void)operationSet;
@end;

@interface SetGameView : PopupView

@property (strong, nonatomic) Game* game;

-(IBAction)closeClicked:(id)sender;
@property (copy, nonatomic) UISlider *laborSlider;
@property (copy, nonatomic) UISlider *milkSlider;
@property (copy, nonatomic) UILabel *laborLabel;
@property (copy, nonatomic) UILabel *milkLabel;
-(IBAction)laborSliderChanged:(id)sender;
-(IBAction) milkSliderChanged:(id)sender;
@property (assign, nonatomic) id delegate;
@end
