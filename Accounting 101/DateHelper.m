//
//  DateHelper.m
//  Accounting 101
//
//  Created by Vijay Thirugnanam on 07/01/14.
//  Copyright (c) 2014 Vijay Thirugnanam. All rights reserved.
//

#import "DateHelper.h"

@implementation DateHelper
+(NSDate *)getDate:(NSInteger)day month:(NSInteger)month year:(NSInteger)year
{
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [[NSDateComponents alloc] init];
    [components setYear:year];
    [components setMonth:month];
    [components setDay:day];
    return [calendar dateFromComponents:components];
}

+(NSDateComponents *)getComponents:(NSDate *)date
{
    NSCalendar* calendar = [NSCalendar currentCalendar];
    NSDateComponents* components = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit|NSWeekdayCalendarUnit fromDate:date];
    return components;
}

+(NSDate *)getFirstDayOfMonth:(NSDate *)date
{
    NSDateComponents *components = [DateHelper getComponents:date];
    return [DateHelper getDate:1 month:components.month year:components.year];
}

+(NSString *)getString:(NSDate *)date format:(NSString *)format
{
    NSDateFormatter * formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:format];
    return [formatter stringFromDate:date];
}

@end
