//
//  SetGameView.m
//  Accounting 101
//
//  Created by Vijay Thirugnanam on 05/01/14.
//  Copyright (c) 2014 Vijay Thirugnanam. All rights reserved.
//

#import "SetGameView.h"
#import "Shop.h"
#import "Game.h"
#import "AudioHelper.h"

@interface SetGameView()
@property (strong, nonatomic) AudioHelper *audioHelper;
@end

@implementation SetGameView
@synthesize game;
@synthesize laborLabel;
@synthesize laborSlider;
@synthesize milkLabel;
@synthesize milkSlider;
@synthesize delegate;
@synthesize audioHelper;

- (id)init
{
    self = [super initWithFrame:CGRectMake(0,0,400,400)];
    if (self) {
        audioHelper = [[AudioHelper alloc] init];
        game = [Game getInstance];
        // Initialization code
        self.backgroundColor = [UIColor colorWithRed:1 green:1 blue:1 alpha:1];
        // Initialization code
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0,0,400,40)];
        titleLabel.backgroundColor = [UIColor colorWithRed:0 green:0.48F blue:1 alpha:1];
        titleLabel.textColor = [UIColor colorWithRed:1 green:1 blue:1 alpha:1];
        titleLabel.text = @"\tSetup Operations";
        [self addSubview:titleLabel];

        UILabel *messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(20,50,360,100)];
        messageLabel.text = [self getMessage];
        messageLabel.numberOfLines = 4;
        [self addSubview:messageLabel];
        
        laborSlider = [[UISlider alloc] initWithFrame:CGRectMake(30, 200, 340, 30)];
        laborSlider.minimumValue = 1;
        laborSlider.maximumValue = 20;
        laborSlider.value = game.labor;
        [laborSlider addTarget:self action:@selector(laborSliderChanged:) forControlEvents:UIControlEventValueChanged];
        [self addSubview:laborSlider];
        
        laborLabel = [[UILabel alloc] initWithFrame:CGRectMake(30, 170, 200, 20)];
        [self laborSliderChanged:nil];
        [self addSubview:laborLabel];

        milkSlider = [[UISlider alloc] initWithFrame:CGRectMake(30, 280, 340, 30)];
        milkSlider.minimumValue = 10;
        milkSlider.maximumValue = 200;
        milkSlider.value = game.milk;
        [milkSlider addTarget:self action:@selector(milkSliderChanged:) forControlEvents:UIControlEventValueChanged];
        [self addSubview:milkSlider];
        
        milkLabel = [[UILabel alloc] initWithFrame:CGRectMake(30, 250, 200, 20)];
        [self milkSliderChanged:nil];
        [self addSubview:milkLabel];
        
        UIButton *closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [closeButton setTitle:@"Close" forState:UIControlStateNormal];
        closeButton.frame = CGRectMake(280,340,100,40);
        closeButton.backgroundColor = [UIColor colorWithRed:0 green:.48 blue:1 alpha:1];
        [closeButton addTarget:self action:@selector(closeClicked:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:closeButton];

    }
    return self;
}

-(NSString *)getMessage
{
    NSString *message = {
        @"The Coffee Shop Game requires labour and milk. "
        @"Set the number of employees you want every day. "
        @"Set the volume of milk your coffee shop requires every day. "
    };
    return message;
}

-(IBAction)closeClicked:(id)sender
{
    [audioHelper playButtonSound];
    Shop *shop = [[Shop alloc] init];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    shop.date = [formatter stringFromDate:game.currentDate];
    shop.seats = game.seats;
    shop.labor = (int)laborSlider.value;
    shop.milk = (int)milkSlider.value;
    [shop save];
    game.labor = shop.labor;
    game.milk = shop.milk;
    if(delegate)
        [delegate operationSet];
    [self hide];
}

-(IBAction)laborSliderChanged:(id)sender
{
    laborSlider.value = round(laborSlider.value);
    laborLabel.text = [NSString stringWithFormat:@"Number of Employees: %.0f", laborSlider.value];
}

-(IBAction)milkSliderChanged:(id)sender
{
    milkSlider.value = round(milkSlider.value);
    milkLabel.text = [NSString stringWithFormat:@"Volume of Milk: %.0f", milkSlider.value];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
