//
//  FirstViewController.h
//  Accounting 101
//
//  Created by Vijay Thirugnanam on 03/01/14.
//  Copyright (c) 2014 Vijay Thirugnanam. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewGameView.h"
#import "SetGameView.h"


@interface GameController : UIViewController<GameCreatedDelegate, OperationSetDelegate, GamePlayedDelegate>
{
    int seats;
    int labor;
    int milk;
    int beans;
    int coffee;
    float balance;
    float sales;
    float expenses;
    bool isPlaying;
    NSDate *currentDate;
}
@property (strong, nonatomic) IBOutlet UILabel *seatLabel;
@property (strong, nonatomic) IBOutlet UILabel *laborLabel;
@property (strong, nonatomic) IBOutlet UILabel *milkLabel;
@property (strong, nonatomic) IBOutlet UILabel *beanLabel;
@property (strong, nonatomic) IBOutlet UILabel *coffeeLabel;
@property (strong, nonatomic) IBOutlet UILabel *dateLabel;
@property (strong, nonatomic) IBOutlet UILabel *balanceLabel;
@property (strong, nonatomic) IBOutlet UILabel *salesLabel;
@property (strong, nonatomic) IBOutlet UIButton *gameButton;
@property (strong, nonatomic) IBOutlet UIButton *changeButton;
@property (strong, nonatomic) IBOutlet UIButton *playButton;
@property (strong, nonatomic) IBOutlet UISegmentedControl *periodSegment;
@property (strong, nonatomic) IBOutlet UISegmentedControl *musicSegment;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIWebView *messageView;

@property (strong, nonatomic) NSDateFormatter *dateFormatter;
@property (strong, nonatomic) NSNumberFormatter *numberFormatter;


-(IBAction)newGameClicked:(id)sender;
-(IBAction)setGameClicked:(id)sender;
-(IBAction)playClicked:(id)sender;

@end
