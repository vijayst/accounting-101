//
//  AudioHelper.m
//  GDP Jarvis
//
//  Created by Vijay Thirugnanam on 25/11/13.
//  Copyright (c) 2013 Vijay Thirugnanam. All rights reserved.
//

#import "AudioHelper.h"

@interface AudioHelper()
-(void)play:(NSURL *)url;
@end

@implementation AudioHelper
@synthesize player;

-(id)init
{
    self = [super init];
    if(self!=nil)
    {
    
    }
    return self;
}

-(void)playDaySound:(NSInteger)type
{
    NSString *resource = [NSString stringWithFormat:@"day%ld", type];
    NSString *path = [[NSBundle mainBundle] pathForResource:resource ofType:@"mp3" inDirectory:@""];
    NSURL *url = [[NSURL alloc] initFileURLWithPath: path];
    [self play:url];
}

-(void)playWeekSound:(NSInteger)type
{
    NSString *resource = [NSString stringWithFormat:@"week%ld", type];
    NSString *path = [[NSBundle mainBundle] pathForResource:resource ofType:@"mp3" inDirectory:@""];
    NSURL *url = [[NSURL alloc] initFileURLWithPath: path];
    [self play:url];
}

-(void)playMonthSound:(NSInteger)type
{
     NSString *resource = [NSString stringWithFormat:@"month%ld", type];
    NSString *path = [[NSBundle mainBundle] pathForResource:resource ofType:@"mp3" inDirectory:@""];
    NSURL *url = [[NSURL alloc] initFileURLWithPath: path];
    [self play:url];
}

-(void)playButtonSound
{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"button" ofType:@"mp3" inDirectory:@""];
    NSURL *url = [[NSURL alloc] initFileURLWithPath: path];
    [self play:url];
}

-(void)playLoadSound
{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"gameload" ofType:@"wav" inDirectory:@""];
    NSURL *url = [[NSURL alloc] initFileURLWithPath: path];
    [self play:url];
}

-(void)play:(NSURL *)url
{
    NSError *error;
    player = [[AVAudioPlayer alloc] initWithContentsOfURL:url error: &error];
    if(error)
        NSLog(@"Error in loading - %@", error.description);
    player.delegate = self;
    [player setVolume:0.2];
    [player play];
}

@end
