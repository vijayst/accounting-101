//
//  MonthCollectionDataSource.h
//  Accounting 101
//
//  Created by Vijay Thirugnanam on 06/01/14.
//  Copyright (c) 2014 Vijay Thirugnanam. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MonthCollectionViewCell.h"


@interface MonthCollectionDataSource : NSObject<UICollectionViewDataSource>
@property (strong, nonatomic) NSMutableArray *monthArray;
@end
