//
//  MonthCollectionViewCell.h
//  Accounting 101
//
//  Created by Vijay Thirugnanam on 06/01/14.
//  Copyright (c) 2014 Vijay Thirugnanam. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MonthCollectionViewCell : UICollectionViewCell
@property (strong, nonatomic) UILabel *label;
@end
