//
//  BalanceSheet.m
//  Accounting 101
//
//  Created by Vijay Thirugnanam on 06/01/14.
//  Copyright (c) 2014 Vijay Thirugnanam. All rights reserved.
//

#import "BalanceSheet.h"
#import "Transaction.h"

@implementation BalanceSheet
@synthesize date;
@synthesize credit;
@synthesize debit;
@synthesize balance;
@synthesize labor;
@synthesize operation;
@synthesize setup;
@synthesize material;
@synthesize milk;
@synthesize bean;

+(BalanceSheet *)compute
{
    BalanceSheet *sheet = [[BalanceSheet alloc] init];
    Transaction *trans = [[Transaction alloc] init];
    sheet.loan = -[trans getDebit:1];
    float setupF = [trans getDebit:2];
    float setupV = [trans getDebit:3];
    sheet.setup = setupF + setupV;
    sheet.labor = [trans getDebit:4];
    float operationF = [trans getDebit:6];
    float operationV = [trans getDebit:7];
    sheet.operation = operationF + operationV;
    sheet.milk = [trans getDebit:8];
    sheet.bean = [trans getDebit:9];
    sheet.sales = -[trans getDebit:10];
    sheet.material = sheet.milk + sheet.bean;
    sheet.credit = sheet.loan + sheet.sales;
    sheet.debit = sheet.setup + sheet.operation + sheet.labor + sheet.material;
    sheet.balance = sheet.credit - sheet.debit;
    return sheet;
}

+(BalanceSheet *)computeFrom:(NSDate *)fromDate toDate:(NSDate *)toDate
{
    BalanceSheet *sheet = [[BalanceSheet alloc] init];
    Transaction *trans = [[Transaction alloc] init];
    sheet.loan = -[trans getDebit:1 from:fromDate to:toDate];
    float setupF = [trans getDebit:2 from:fromDate to:toDate];
    float setupV = [trans getDebit:3 from:fromDate to:toDate];
    sheet.setup = setupF + setupV;
    sheet.labor = [trans getDebit:4 from:fromDate to:toDate];
    float operationF = [trans getDebit:6 from:fromDate to:toDate];
    float operationV = [trans getDebit:7 from:fromDate to:toDate];
    sheet.operation = operationF + operationV;
    sheet.milk = [trans getDebit:8 from:fromDate to:toDate];
    sheet.bean = [trans getDebit:9 from:fromDate to:toDate];
    sheet.sales = -[trans getDebit:10 from:fromDate to:toDate];
    sheet.material = sheet.milk + sheet.bean;
    sheet.credit = sheet.loan + sheet.sales;
    sheet.debit = sheet.setup + sheet.operation + sheet.labor + sheet.material;
    sheet.balance = sheet.credit - sheet.debit;
    return sheet;

}

@end
