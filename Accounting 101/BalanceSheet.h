//
//  BalanceSheet.h
//  Accounting 101
//
//  Created by Vijay Thirugnanam on 06/01/14.
//  Copyright (c) 2014 Vijay Thirugnanam. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BalanceSheet : NSObject
@property NSDate *date;
@property float credit;
@property float debit;
@property float balance;

@property float loan;
@property float sales;
@property float setup;
@property float labor;
@property float material;
@property float bean;
@property float milk;
@property float operation;

+(BalanceSheet *)compute;
+(BalanceSheet *)computeFrom:(NSDate *)fromDate toDate:(NSDate *)toDate;
@end
