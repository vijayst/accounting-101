//
//  FirstViewController.m
//  Accounting 101
//
//  Created by Vijay Thirugnanam on 03/01/14.
//  Copyright (c) 2014 Vijay Thirugnanam. All rights reserved.
//

#import "GameController.h"
#import "NewGameView.h"
#import "SetGameView.h"
#import "PlayView.h"
#import "Game.h"
#import "AudioHelper.h"

@interface GameController ()
@property (strong, nonatomic) AudioHelper *audioHelper;
@property (strong, nonatomic) PlayView *playView;
-(void)updateCash;
-(void)updateProfit;
-(void)updateLabels;
@end

@implementation GameController
@synthesize seatLabel;
@synthesize laborLabel;
@synthesize milkLabel;
@synthesize beanLabel;
@synthesize coffeeLabel;
@synthesize periodSegment;
@synthesize musicSegment;

@synthesize scrollView;
@synthesize messageView;

@synthesize playButton;
@synthesize gameButton;
@synthesize changeButton;

@synthesize dateLabel;
@synthesize balanceLabel;
@synthesize salesLabel;

@synthesize dateFormatter;
@synthesize numberFormatter;
@synthesize audioHelper;
@synthesize playView;


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd MMM\nyyyy"];
    
    numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    [numberFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    
    audioHelper = [[AudioHelper alloc] init];
	
    Game *game = [Game getGameFromDb];
    if(game==nil) {
        game = [Game createGame:20 date:[NSDate date]];
    }
    seats = game.seats;
    labor = game.labor;
    milk = game.milk;
    beans = game.beans;
    coffee = game.coffee;
    balance = game.balance;
    sales = game.revenues;
    expenses = game.expenses;
    currentDate = game.currentDate;
    
    game.delegate = self;

    seatLabel.backgroundColor = [UIColor colorWithRed:0.5 green:187/256.0 blue:0 alpha:1];
    laborLabel.backgroundColor = [UIColor colorWithRed:0.5 green:187/256.0 blue:0 alpha:1];
    milkLabel.backgroundColor = [UIColor colorWithRed:0.5 green:187/256.0 blue:0 alpha:1];
    beanLabel.backgroundColor = [UIColor colorWithRed:0.5 green:187/256.0 blue:0 alpha:1];
    coffeeLabel.backgroundColor = [UIColor colorWithRed:0.5 green:187/256.0 blue:0 alpha:1];
    
    dateLabel.layer.borderColor = [[UIColor blackColor] CGColor];
    dateLabel.layer.borderWidth = 2.0F;
    
    balanceLabel.layer.borderWidth = 2.0F;
    salesLabel.layer.borderWidth = 2.0F;
    
    [periodSegment removeAllSegments];
    [periodSegment setFrame:CGRectMake(periodSegment.frame.origin.x, periodSegment.frame.origin.y, periodSegment.frame.size.width, 50.0F)];
    [periodSegment insertSegmentWithTitle:@"Day" atIndex:0 animated:FALSE];
    [periodSegment insertSegmentWithTitle:@"Week" atIndex:1 animated:FALSE];
    [periodSegment insertSegmentWithTitle:@"Month" atIndex:2 animated:FALSE];
    periodSegment.selectedSegmentIndex = 1;
    
    [musicSegment removeAllSegments];
    [musicSegment setFrame:CGRectMake(musicSegment.frame.origin.x, musicSegment.frame.origin.y, musicSegment.frame.size.width, 50.0F)];
    [musicSegment insertSegmentWithTitle:@"Zimmer" atIndex:0 animated:FALSE];
    [musicSegment insertSegmentWithTitle:@"Ilayaraja" atIndex:1 animated:FALSE];
    [musicSegment insertSegmentWithTitle:@"Burman" atIndex:2 animated:FALSE];
     musicSegment.selectedSegmentIndex = 0;
    
    [gameButton addTarget:self action:@selector(newGameClicked:) forControlEvents:UIControlEventTouchUpInside];
    [changeButton addTarget:self action:@selector(setGameClicked:) forControlEvents:UIControlEventTouchUpInside];
    [playButton addTarget:self action:@selector(playClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    [self updateLabels];
    [self setMessage:[game getMessage]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)newGameClicked:(id)sender
{
    [audioHelper playButtonSound];
    NewGameView *firstView = [[NewGameView alloc] init];
    firstView.delegate = self;
    [firstView show];
}

-(void)gameCreated
{
    Game *game = [Game getInstance];
    seats = game.seats;
    labor = game.labor;
    milk = game.milk;
    beans = game.beans;
    coffee = game.coffee;
    balance = game.balance;
    sales = game.revenues;
    expenses = game.expenses;
    currentDate = game.currentDate;
    [self updateLabels];
    [self setMessage:[game getMessage]];
    game.delegate = self;
}

-(IBAction)setGameClicked:(id)sender
{
    [audioHelper playButtonSound];
    SetGameView *secondView = [[SetGameView alloc] init];
    secondView.delegate = self;
    [secondView show];
}

-(void)operationSet
{
    Game *game = [Game getInstance];
    labor = game.labor;
    laborLabel.attributedText = [self getLaborLabel];
    milk = game.milk;
    milkLabel.attributedText = [self getMilkLabel];
}

const float intervalArray[3][3] = { {6,2,1.06}, {6,2.3,1.1}, {5,1.9,1.1} };

-(IBAction)playClicked:(id)sender
{
    Game *game = [Game getInstance];
    int days = 1;
    switch(periodSegment.selectedSegmentIndex)
    {
        case 0:
            days = 1;
            break;
        case 1:
            days = 7;
            break;
        case 2:
            days = 30;
            break;
    }
    
    float interval = intervalArray[musicSegment.selectedSegmentIndex][periodSegment.selectedSegmentIndex];
    [game playGame:days audioType:(musicSegment.selectedSegmentIndex+1) interval:interval];
}

-(void)gamePlayStarted
{
    if(playView==nil) {
        playView = [[PlayView alloc] init];
        CGRect windowBounds =[[UIScreen mainScreen] bounds];
        playView.windowFrame = CGRectMake(windowBounds.origin.x,windowBounds.origin.y,windowBounds.size.width,scrollView.frame.origin.y);
    }
     [playView show];
    UITabBarController *tabController = (UITabBarController *)self.parentViewController;
    tabController.view.userInteractionEnabled = false;
     isPlaying = true;
}

-(void)gamePlayed:(Game *)game
{
    [self setMessage:[game getMessage]];
    balance = game.balance;
    expenses = game.expenses;
    sales = game.revenues;
    [self updateCash];
    [self updateProfit];
    beans = (int)game.beans;
    beanLabel.attributedText = [self getBeanLabel];
    coffee = game.coffee;
    coffeeLabel.attributedText = [self getCoffeeLabel];
    dateLabel.attributedText = [self getDateLabel:[game currentDate]];
}

-(void)gamePlayEnded
{
    UITabBarController *tabController = (UITabBarController *)self.parentViewController;
    tabController.view.userInteractionEnabled = true;
    [playView hide];
    isPlaying = false;
}

-(void)updateCash
{
    balanceLabel.attributedText = [self getBalanceLabel];
    if(balance>=0) {
        balanceLabel.backgroundColor = [UIColor colorWithRed:0 green:1 blue:0 alpha:0.5];
        balanceLabel.layer.borderColor = [[UIColor greenColor] CGColor];
    } else {
        balanceLabel.backgroundColor = [UIColor colorWithRed:1 green:0 blue:0 alpha:0.5];
        balanceLabel.layer.borderColor = [[UIColor redColor] CGColor];
    }
}

-(void)updateProfit
{
    salesLabel.attributedText = [self getSalesLabel];
    float profit = sales - expenses;
    if(profit>=0) {
        salesLabel.backgroundColor = [UIColor colorWithRed:0 green:1 blue:0 alpha:0.5];
        salesLabel.layer.borderColor = [[UIColor greenColor] CGColor];
    } else {
        salesLabel.backgroundColor = [UIColor colorWithRed:1 green:0 blue:0 alpha:0.5];
        salesLabel.layer.borderColor = [[UIColor redColor] CGColor];
    }

}

-(void)updateLabels
{
    seatLabel.attributedText = [self getSeatLabel];
    laborLabel.attributedText = [self getLaborLabel];
    milkLabel.attributedText = [self getMilkLabel];
    beanLabel.attributedText = [self getBeanLabel];
    coffeeLabel.attributedText = [self getCoffeeLabel];
    dateLabel.attributedText = [self getDateLabel:currentDate];
    [self updateCash];
    [self updateProfit];
}

-(NSAttributedString *)getSeatLabel
{
    NSMutableAttributedString *seatText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"Seats\n%d", seats]];
    [seatText addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:25.0F] range:NSMakeRange(0,5)];
    return seatText;
}

-(NSAttributedString *)getLaborLabel
{
    NSMutableAttributedString *laborText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"Labor\n%d", labor]];
    [laborText addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:25.0F] range:NSMakeRange(0,5)];
    return laborText;
}

-(NSAttributedString *)getMilkLabel
{
    NSMutableAttributedString *milkText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"Milk\n%d", milk]];
    [milkText addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:25.0F] range:NSMakeRange(0,4)];
    return milkText;
}

-(NSAttributedString *)getBeanLabel
{
    NSMutableAttributedString *beanText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"Bean\n%d", beans]];
    [beanText addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:25.0F] range:NSMakeRange(0,4)];
    return beanText;
}

-(NSAttributedString *)getCoffeeLabel
{
    NSMutableAttributedString *coffeeText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"Coffee\n%d", coffee]];
    [coffeeText addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:25.0F] range:NSMakeRange(0,6)];
    return coffeeText;
}

-(NSAttributedString *)getDateLabel:(NSDate *)date
{
    NSString *formattedDate = [dateFormatter stringFromDate:date];
    NSMutableAttributedString *dateText = [[NSMutableAttributedString alloc] initWithString:formattedDate];
    NSRange range = NSMakeRange(0, ([formattedDate rangeOfCharacterFromSet:[NSCharacterSet characterSetWithCharactersInString:@"\n"]]).location);
    [dateText addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:25.0F] range:range];
    return dateText;
}

-(NSAttributedString *)getBalanceLabel
{
    NSString *formattedBalance = [numberFormatter stringFromNumber:[NSNumber numberWithFloat:balance]];
    NSMutableAttributedString *balanceText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"Cash\n%@", formattedBalance]];
    [balanceText addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:25.0F] range:NSMakeRange(0,4)];
    return balanceText;
}

-(NSAttributedString *)getSalesLabel
{
    float profit = sales-expenses;
    NSString *formattedSales = [numberFormatter stringFromNumber:[NSNumber numberWithFloat:profit]];
    NSMutableAttributedString *salesText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"Profit\n%@", formattedSales]];
    [salesText addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:25.0F] range:NSMakeRange(0,6)];
    return salesText;
}

-(NSAttributedString *)getExpensesLabel
{
    NSString *formattedExpenses = [numberFormatter stringFromNumber:[NSNumber numberWithFloat:expenses]];
    NSMutableAttributedString *expensesText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"Expenses\n%@", formattedExpenses]];
    [expensesText addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:25.0F] range:NSMakeRange(0,8)];
    return expensesText;
}

-(void)setMessage:(NSString *)message
{
    NSString *htmlTemplate = {
        @"<html>"
        @"<head>"
        @"<style>"
        @"body { background-color: #C6E2FF; margin-left: 50px;}"
        @"h1 { color: #0070FF; font-size: 40px; }"
        @"div { margin: 2px 20px; padding: 0px; font-size: 24px;}"
        @"</style>"
        @"</head>"
        @"<body>%@</body>"
        @"</html>"
    };
    [messageView loadHTMLString:[NSString stringWithFormat:htmlTemplate, message] baseURL:nil];
}

@end
