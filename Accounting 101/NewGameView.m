//
//  NewGameView.m
//  Accounting 101
//
//  Created by Vijay Thirugnanam on 05/01/14.
//  Copyright (c) 2014 Vijay Thirugnanam. All rights reserved.
//

#import "NewGameView.h"
#import "DbHelper.h"
#import "Shop.h"
#import "Game.h"
#import "AudioHelper.h"

@interface NewGameView()
@property (strong, nonatomic) AudioHelper *audioHelper;
@end

@implementation NewGameView

@synthesize slider;
@synthesize seatLabel;
@synthesize delegate;
@synthesize  audioHelper;

- (id)init
{
    self = [super initWithFrame:CGRectMake(0,0,400,400)];
    if (self) {
        audioHelper = [[AudioHelper alloc] init];
        self.backgroundColor = [UIColor colorWithRed:1 green:1 blue:1 alpha:1];
        // Initialization code
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0,0,400,40)];
        titleLabel.backgroundColor = [UIColor colorWithRed:0 green:0.48F blue:1 alpha:1];
        titleLabel.textColor = [UIColor colorWithRed:1 green:1 blue:1 alpha:1];
        titleLabel.text = @"\tSetup Coffee Shop";
        [self addSubview:titleLabel];
        
        UIEdgeInsets messageInsets = UIEdgeInsetsMake(10, 20, 10, 20);
        CGRect messageFrame = UIEdgeInsetsInsetRect(CGRectMake(0,40,400,250), messageInsets);
        UILabel *messageLabel = [[UILabel alloc] initWithFrame:messageFrame];
        messageLabel.text = [self getMessage];
        messageLabel.numberOfLines = 12;
        [self addSubview:messageLabel];
        
        slider = [[UISlider alloc] initWithFrame:CGRectMake(20, 310, 360, 30)];
        slider.minimumValue = 20;
        slider.maximumValue = 80;
        slider.value = 20;
        [slider addTarget:self action:@selector(sliderChanged:) forControlEvents:UIControlEventValueChanged];
        [self addSubview:slider];
        
        seatLabel = [[UILabel alloc] initWithFrame:CGRectMake(30, 350, 200, 40)];
        seatLabel.text = [NSString stringWithFormat:@"Number of Seats: %.0f", slider.value];
        [self addSubview:seatLabel];
        
        UIButton *closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [closeButton setTitle:@"Close" forState:UIControlStateNormal];
        closeButton.frame = CGRectMake(290,350,100,40);
        closeButton.backgroundColor = [UIColor colorWithRed:0 green:.48 blue:1 alpha:1];
        [closeButton addTarget:self action:@selector(closeClicked:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:closeButton];
    }
    return self;
}

-(NSString *)getMessage
{
    NSString *message = {
        @"The Coffee Shop Game helps you understand the basics of Accounting. "
        @"You have a loan of $100,000 to setup a coffee shop. "
        @"To setup the coffee shop, you need atleast $50,000. "
        @"You also have to decide how big the coffee shop is. "
        @"A small coffee shop with 20 seats will cost you an additional $10,000. "
        @"A large coffee shop with 80 seats will cost you an additional $40,000. "
        @"Please select the number of seats to begin the game."
    };
    return message;
}

-(IBAction)sliderChanged:(id)sender
{
    slider.value = round(slider.value);
    seatLabel.text = [NSString stringWithFormat:@"Number of Seats: %.0f", slider.value];
}

-(IBAction)closeClicked:(id)sender
{
    [audioHelper playButtonSound];
    [Game reset];
    Shop *shop = [[Shop alloc] init];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    shop.date = [formatter stringFromDate:[NSDate date]];
    shop.seats = (int)slider.value;
    shop.labor = 1;
    shop.milk = 10;
    [shop save];
    [Game createGame:shop.seats date:[NSDate date]];
    if(delegate)
        [delegate gameCreated];
    [self hide];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
