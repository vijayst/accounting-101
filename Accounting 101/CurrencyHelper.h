//
//  CurrencyHelper.h
//  Accounting 101
//
//  Created by Vijay Thirugnanam on 07/01/14.
//  Copyright (c) 2014 Vijay Thirugnanam. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CurrencyHelper : NSObject
+(NSString *)formatNumber:(float)number;
@end
