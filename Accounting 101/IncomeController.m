//
//  IncomeController.m
//  Accounting 101
//
//  Created by Vijay Thirugnanam on 03/01/14.
//  Copyright (c) 2014 Vijay Thirugnanam. All rights reserved.
//

#import "IncomeController.h"
#import "IncomeStatement.h"
#import "Game.h"
#import "MonthCollectionDataSource.h"
#import "DateHelper.h"

@interface IncomeController ()
@property (strong, nonatomic) MonthCollectionDataSource *monthSource;
-(void)computeIncomeStatement:(NSDate *)pdate;
@end

@implementation IncomeController
@synthesize monthLabel;
@synthesize salesLabel;
@synthesize revenueLabel;
@synthesize fixedLabel;
@synthesize laborLabel;
@synthesize materialLabel;
@synthesize milkLabel;
@synthesize beansLabel;
@synthesize expenseLabel;
@synthesize profitLabel;
@synthesize monthCollectionView;
@synthesize monthSource;
@synthesize creditView;
@synthesize headerView;
@synthesize debitView;
@synthesize totalView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
       
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    headerView.backgroundColor =  [UIColor colorWithRed:0 green:115/256.0 blue:199/256.0 alpha:1];
    creditView.backgroundColor = [UIColor colorWithRed:0.5 green:187/256.0 blue:0 alpha:1];
    debitView.backgroundColor = [UIColor colorWithRed:1 green:242/256.0 blue:0 alpha:1];
    totalView.backgroundColor = [UIColor colorWithRed:222/256.0 green:90/256.0 blue:0 alpha:1];
    
    [monthCollectionView registerClass:[MonthCollectionViewCell class] forCellWithReuseIdentifier:@"aCell"];
    monthSource = [[MonthCollectionDataSource alloc] init];
    monthCollectionView.dataSource = monthSource;
    monthCollectionView.delegate = self;
}

-(void)viewDidAppear:(BOOL)animated
{
    Game *game = [Game getInstance];
    [self computeIncomeStatement:[game currentDate]];
    [monthCollectionView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// UICollectionViewDelegateFlowLayout
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGSize retval = CGSizeMake(200, 60);
    return retval;
}

- (UIEdgeInsets)collectionView:
(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(20, 20, 20, 20);
}

// UICollectionViewDelegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSDate *date = [monthSource.monthArray objectAtIndex:indexPath.row];
    [self computeIncomeStatement:date];
}
- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath {
    // TODO: Deselect item
}

-(void)computeIncomeStatement:(NSDate *)pdate
{
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    [numberFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];

    NSDateComponents *components = [DateHelper getComponents:pdate];
    NSDateFormatter * formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MMMM yyyy"];
    monthLabel.text = [formatter stringFromDate:pdate];
    
    IncomeStatement *income = [IncomeStatement compute:components.year month:components.month];
    salesLabel.text = [numberFormatter stringFromNumber:[NSNumber numberWithFloat:income.sales]];
    revenueLabel.text = [numberFormatter stringFromNumber:[NSNumber numberWithFloat:income.revenues]];
    fixedLabel.text = [numberFormatter stringFromNumber:[NSNumber numberWithFloat:income.operation]];
    laborLabel.text = [numberFormatter stringFromNumber:[NSNumber numberWithFloat:income.labor]];
    milkLabel.text = [numberFormatter stringFromNumber:[NSNumber numberWithFloat:income.milk]];
    beansLabel.text = [numberFormatter stringFromNumber:[NSNumber numberWithFloat:income.beans]];
    materialLabel.text = [numberFormatter stringFromNumber:[NSNumber numberWithFloat:income.material]];
    expenseLabel.text = [numberFormatter stringFromNumber:[NSNumber numberWithFloat:income.expenses]];
    profitLabel.text = [numberFormatter stringFromNumber:[NSNumber numberWithFloat:income.profit]];
}

@end
