//
//  Shop.h
//  Accounting 101
//
//  Created by Vijay Thirugnanam on 06/01/14.
//  Copyright (c) 2014 Vijay Thirugnanam. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Shop : NSObject
{

}

@property NSString *date;
@property int seats;
@property int labor;
@property int milk;

+(Shop *)getShop:(NSString *)date;
+(Shop *)getLatest;
+(void)reset;
-(void)save;
-(void)insert;
-(void)update;
@end
