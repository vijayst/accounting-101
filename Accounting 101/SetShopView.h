//
//  SetShopView.h
//  Accounting 101
//
//  Created by Vijay Thirugnanam on 05/01/14.
//  Copyright (c) 2014 Vijay Thirugnanam. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SetShopView : UIAlertView<UIAlertViewDelegate>
@property(weak, nonatomic) UIButton *setButton;
@end
