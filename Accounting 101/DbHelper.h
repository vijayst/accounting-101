//
//  DbHelper.h
//  Accounting 101
//
//  Created by Vijay Thirugnanam on 05/01/14.
//  Copyright (c) 2014 Vijay Thirugnanam. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>

@interface DbHelper : NSObject
{
    sqlite3 *database;
}
-(bool)executeCommand:(NSString *)sql;
-(bool)executeQuery:(NSString *)sql statement:(sqlite3_stmt **)statement;
-(float)executeReal:(NSString *)sql;
+(DbHelper *)getInstance;
@end
