//
//  SecondViewController.h
//  Accounting 101
//
//  Created by Vijay Thirugnanam on 03/01/14.
//  Copyright (c) 2014 Vijay Thirugnanam. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BalanceController : UIViewController
@property (strong, nonatomic) IBOutlet UILabel *cashLabel;
@property (strong, nonatomic) IBOutlet UILabel *loanLabel;
@property (strong, nonatomic) IBOutlet UILabel *creditLabel;
@property (strong, nonatomic) IBOutlet UILabel *setupLabel;
@property (strong, nonatomic) IBOutlet UILabel *debitLabel;
@property (strong, nonatomic) IBOutlet UILabel *balanceLabel;
@property (strong, nonatomic) IBOutlet UILabel *dateLabel;
@property (strong, nonatomic) IBOutlet UIView *creditView;
@property (strong, nonatomic) IBOutlet UIView *headerView;
@property (strong, nonatomic) IBOutlet UIView *debitView;
@property (strong, nonatomic) IBOutlet UIView *totalView;
@property (strong, nonatomic) IBOutlet UIDatePicker *datePicker;
- (IBAction)dateChanged:(id)sender;
@end
