//
//  SecondViewController.m
//  Accounting 101
//
//  Created by Vijay Thirugnanam on 03/01/14.
//  Copyright (c) 2014 Vijay Thirugnanam. All rights reserved.
//

#import "BalanceController.h"
#import "BalanceSheet.h"
#import "Game.h"
#import "DateHelper.h"

@interface BalanceController ()
@property (strong, nonatomic) NSNumberFormatter *numberFormatter;
@property (strong, nonatomic) Game* game;
-(void)updateView:(BalanceSheet *)sheet;
@end

@implementation BalanceController
@synthesize loanLabel;
@synthesize creditLabel;
@synthesize setupLabel;
@synthesize debitLabel;
@synthesize balanceLabel;
@synthesize dateLabel;
@synthesize creditView;
@synthesize headerView;
@synthesize debitView;
@synthesize totalView;
@synthesize cashLabel;
@synthesize datePicker;

@synthesize numberFormatter;
@synthesize game;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    [numberFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    
    game = [Game getInstance];
    [datePicker setMinimumDate:game.startDate];
}

-(void)viewDidAppear:(BOOL)animated
{
    dateLabel.text = [DateHelper getString:game.currentDate format:@"d MMMM yyyy"];
    [datePicker setDate:game.currentDate];
    [datePicker setMaximumDate:game.currentDate];
    BalanceSheet *sheet = [BalanceSheet compute];
    [self updateView:sheet];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)dateChanged:(id)sender {
    dateLabel.text = [DateHelper getString:[datePicker date] format:@"d MMM yyyy"];
    BalanceSheet *sheet = [BalanceSheet computeFrom:[game startDate] toDate:[datePicker date]];
    [self updateView:sheet];
}

-(void)updateView:(BalanceSheet *)sheet
{
    headerView.backgroundColor =  [UIColor colorWithRed:0 green:115/256.0 blue:199/256.0 alpha:1];
    creditView.backgroundColor = [UIColor colorWithRed:0.5 green:187/256.0 blue:0 alpha:1];
    debitView.backgroundColor = [UIColor colorWithRed:1 green:242/256.0 blue:0 alpha:1];
    totalView.backgroundColor = [UIColor colorWithRed:222/256.0 green:90/256.0 blue:0 alpha:1];
    
    cashLabel.text = [numberFormatter stringFromNumber:[NSNumber numberWithFloat:sheet.balance]];
    loanLabel.text = [numberFormatter stringFromNumber:[NSNumber numberWithFloat:sheet.loan]];
    float assets = sheet.balance + sheet.setup;
    creditLabel.text = [numberFormatter stringFromNumber:[NSNumber numberWithFloat:assets]];
    setupLabel.text = [numberFormatter stringFromNumber:[NSNumber numberWithFloat:sheet.setup]];
    debitLabel.text = [numberFormatter stringFromNumber:[NSNumber numberWithFloat:sheet.loan]];
    float profit = sheet.balance + sheet.setup - sheet.loan;
    balanceLabel.text = [numberFormatter stringFromNumber:[NSNumber numberWithFloat:profit]];
}

@end
