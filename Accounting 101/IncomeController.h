//
//  IncomeController.h
//  Accounting 101
//
//  Created by Vijay Thirugnanam on 03/01/14.
//  Copyright (c) 2014 Vijay Thirugnanam. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IncomeController : UIViewController<UICollectionViewDelegateFlowLayout>
@property (strong, nonatomic) IBOutlet UILabel *monthLabel;
@property (strong, nonatomic) IBOutlet UILabel *salesLabel;
@property (strong, nonatomic) IBOutlet UILabel *revenueLabel;
@property (strong, nonatomic) IBOutlet UILabel *fixedLabel; // operation
@property (strong, nonatomic) IBOutlet UILabel *laborLabel;
@property (strong, nonatomic) IBOutlet UILabel *materialLabel;
@property (strong, nonatomic) IBOutlet UILabel *milkLabel;
@property (strong, nonatomic) IBOutlet UILabel *beansLabel;
@property (strong, nonatomic) IBOutlet UILabel *expenseLabel;
@property (strong, nonatomic) IBOutlet UILabel *profitLabel;
@property (strong, nonatomic) IBOutlet UICollectionView *monthCollectionView;
@property (strong, nonatomic) IBOutlet UIView *headerView;
@property (strong, nonatomic) IBOutlet UIView *creditView;
@property (strong, nonatomic) IBOutlet UIView *debitView;
@property (strong, nonatomic) IBOutlet UIView *totalView;
@end
