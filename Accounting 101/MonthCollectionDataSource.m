//
//  MonthCollectionDataSource.m
//  Accounting 101
//
//  Created by Vijay Thirugnanam on 06/01/14.
//  Copyright (c) 2014 Vijay Thirugnanam. All rights reserved.
//

#import "MonthCollectionDataSource.h"
#import "Game.h"
#import "DateHelper.h"

@implementation MonthCollectionDataSource
@synthesize monthArray;
-(id)init
{
    self = [super init];
    if(self) {
        
    }
    return self;
}

// UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    monthArray = [[NSMutableArray alloc] init];
    Game *game = [Game getInstance];
    NSDate *monthDate = game.currentDate;
    NSDateComponents *components = [DateHelper getComponents:monthDate];
    monthDate = [DateHelper getFirstDayOfMonth:monthDate];
    NSDate *startDate = [DateHelper getFirstDayOfMonth:game.startDate];
    do
    {
        [monthArray addObject:monthDate];
        if(components.month==1) {
            components.year--;
            components.month = 12;
        }
        else
            components.month--;
        monthDate = [DateHelper getDate:1 month:components.month year:components.year];
    } while([monthDate compare:startDate]!=NSOrderedAscending);
    return [monthArray count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    MonthCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"aCell" forIndexPath:indexPath];
    NSDate *date = [monthArray objectAtIndex:indexPath.row];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MMMM yyyy"];
    cell.label.text = [formatter stringFromDate:date];
    return cell;
}


@end
