//
//  CurrencyHelper.m
//  Accounting 101
//
//  Created by Vijay Thirugnanam on 07/01/14.
//  Copyright (c) 2014 Vijay Thirugnanam. All rights reserved.
//

#import "CurrencyHelper.h"

@implementation CurrencyHelper
+(NSString *)formatNumber:(float)number
{
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    [numberFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    [numberFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    return [numberFormatter stringFromNumber:[NSNumber numberWithFloat:number]];
}
@end
