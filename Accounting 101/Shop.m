//
//  Shop.m
//  Accounting 101
//
//  Created by Vijay Thirugnanam on 06/01/14.
//  Copyright (c) 2014 Vijay Thirugnanam. All rights reserved.
//

#import "Shop.h"
#import <sqlite3.h>
#import "DbHelper.h"

@interface Shop()
@property (strong, nonatomic) DbHelper *helper;
@end

@implementation Shop
@synthesize helper;

@synthesize date;
@synthesize seats;
@synthesize labor;
@synthesize milk;

-(id)init
{
    self = [super init];
    if(self)
    {
        helper = [DbHelper getInstance];
    }
    return self;
}

+(void)reset
{
    DbHelper *lhelper = [DbHelper getInstance];
    NSString *sql = [NSString stringWithFormat:@"DELETE FROM shop"];
    [lhelper executeCommand:sql];
}

+(Shop *)getShop:(NSString *)pdate
{
    DbHelper *lhelper = [DbHelper getInstance];
    NSString *sql = [NSString stringWithFormat:@"SELECT seats, labor, milk FROM SHOP WHERE date = '%@'", pdate];
    sqlite3_stmt *statement = nil;
    Shop *shop = nil;
    if([lhelper executeQuery:sql statement:&statement])
    {
        while (sqlite3_step(statement) == SQLITE_ROW) {
            shop = [[Shop alloc] init];
            shop.date = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(statement, 0)];
            shop.seats = sqlite3_column_int(statement, 1);
            shop.labor = sqlite3_column_int(statement, 2);
            shop.milk = sqlite3_column_int(statement, 3);
        }
        sqlite3_finalize(statement);
    }
    return shop;
}

+(Shop *)getLatest
{
    DbHelper *lhelper = [DbHelper getInstance];
    NSString *sql = [NSString stringWithFormat:@"SELECT date([date]), seats, labor, milk FROM shop ORDER BY [date] DESC"];
    sqlite3_stmt *statement = nil;
    Shop *shop = nil;
    if([lhelper executeQuery:sql statement:&statement])
    {
        if(sqlite3_step(statement) == SQLITE_ROW) {
            shop = [[Shop alloc] init];
            shop.date = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(statement, 0)];
            shop.seats = sqlite3_column_int(statement, 1);
            shop.labor = sqlite3_column_int(statement, 2);
            shop.milk = sqlite3_column_int(statement, 3);
        }
        sqlite3_finalize(statement);
    }
    return shop;
}

-(void)save
{
    bool shopExists = [Shop getShop:date]!=nil;
    if(shopExists)
        [self update];
    else
        [self insert];
}

-(void)insert
{
    NSString *sql = [NSString stringWithFormat:@"INSERT INTO SHOP (date, seats, labor, milk) VALUES ('%@', %d, %d, %d)", date, seats, labor, milk];
    [helper executeCommand:sql];
}

-(void)update
{
    NSString *sql = [NSString stringWithFormat:@"UPDATE SHOP SET seats = %d, labor = %d, milk = %d WHERE date = '%@'", seats, labor, milk, date];
    [helper executeCommand:sql];
}

@end
