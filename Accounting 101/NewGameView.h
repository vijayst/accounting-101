//
//  NewGameView.h
//  Accounting 101
//
//  Created by Vijay Thirugnanam on 05/01/14.
//  Copyright (c) 2014 Vijay Thirugnanam. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PopupView.h"

@protocol GameCreatedDelegate
@optional
-(void)gameCreated;
@end

@interface NewGameView : PopupView
@property(copy, nonatomic) UISlider *slider;
@property(copy, nonatomic) UILabel *seatLabel;
-(IBAction)sliderChanged:(id)sender;
-(IBAction)closeClicked:(id)sender;
@property (assign, nonatomic) id delegate;
@end
