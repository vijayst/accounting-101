//
//  DateHelper.h
//  Accounting 101
//
//  Created by Vijay Thirugnanam on 07/01/14.
//  Copyright (c) 2014 Vijay Thirugnanam. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DateHelper : NSObject
+(NSDate *)getDate:(NSInteger)day month:(NSInteger)month year:(NSInteger)year;
+(NSDateComponents *)getComponents:(NSDate *)date;
+(NSDate *)getFirstDayOfMonth:(NSDate *)date;
+(NSString *)getString:(NSDate *)date format:(NSString *)format;
@end
