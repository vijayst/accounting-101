//
//  PopupView.m
//  Accounting 101
//
//  Created by Vijay Thirugnanam on 05/01/14.
//  Copyright (c) 2014 Vijay Thirugnanam. All rights reserved.
//

#import "PopupView.h"

@interface PopupView()
@property (strong, nonatomic) UIWindow *window;
@end

@implementation PopupView

@synthesize window;
@synthesize windowFrame;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.windowFrame = [[UIScreen mainScreen] bounds];
    }
    return self;
}

-(void)show
{
    self.window = [[UIWindow alloc] initWithFrame:self.windowFrame];
    self.window.windowLevel = UIWindowLevelAlert;
    self.window.backgroundColor = [UIColor colorWithRed:1 green:1 blue:1 alpha:0.5];
    
    self.center = CGPointMake(CGRectGetMidX(self.window.bounds), CGRectGetMidY(self.window.bounds));
    [self.window addSubview:self];
    [self.window makeKeyAndVisible];
}

-(void)hide
{
    self.window.hidden = YES;
    self.window = nil;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
