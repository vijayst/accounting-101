//
//  IncomeStatement.h
//  Accounting 101
//
//  Created by Vijay Thirugnanam on 06/01/14.
//  Copyright (c) 2014 Vijay Thirugnanam. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IncomeStatement : NSObject
@property int month;
@property int year;
@property float sales;
@property float revenues;
@property float operation;
@property float labor;
@property float material;
@property float milk;
@property float beans;
@property float expenses;
@property float profit;

+(IncomeStatement *)compute:(NSInteger)year month:(NSInteger)month;
@end
