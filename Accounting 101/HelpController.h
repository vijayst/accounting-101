//
//  HelpController.h
//  Accounting 101
//
//  Created by Vijay Thirugnanam on 03/01/14.
//  Copyright (c) 2014 Vijay Thirugnanam. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HelpController : UIViewController
@property (strong, nonatomic) IBOutlet UIWebView *contentView;
@end
