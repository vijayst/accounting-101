//
//  HelpController.m
//  Accounting 101
//
//  Created by Vijay Thirugnanam on 03/01/14.
//  Copyright (c) 2014 Vijay Thirugnanam. All rights reserved.
//

#import "HelpController.h"

@interface HelpController ()

@end

@implementation HelpController
@synthesize contentView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [contentView loadHTMLString:[self getHelpHtml] baseURL:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSString*)getHelpHtml
{
    NSString *html = {
        @"<html>"
        @"<head>"
        @"<style>"
        @"body { margin: 10px 50px;}"
        @"h1 { color: #0070FF; font-size: 40px; text-align: center; }"
        @"h2 { color: #0070FF; font-size: 32px; }"
        @"p { margin: 5px 20px; padding: 0px; font-size: 24px;}"
        @"li { font-size: 24px; }"
        @"</style>"
        @"</head>"
        @"<body>"
        @"<div style=\"margin: 20px\">"
        @"<h1>Accounting 101</h1>"
        @"<h2>Coffee shop</h2>"
        @"<p>You are the owner of a coffee shop. The game lets you:"
        @"<ul>"
        @"<li>Setup the coffee shop.</li>"
        @"<li>Setup the operations of the coffee shop.</li>"
        @"<li>Monitor your balance sheet and income statements.</li>"
        @"</ul>"
        @"</p>"
        @"<h2>Setup the coffee shop</h2>"
        @"<p>"
        @"The coffee shop is located in a town of 100,000 people. "
        @"You have $100,000 of cash as loan to set the coffee shop. "
        @"Setting up the coffee shop requires $50,000. "
        @"But wait! That is not all it. "
        @"The coffee shop can be big or small. "
        @"A coffee shop can have a capacity as low as 20 seats. "
        @"Or the coffee shop can have a capacity as high as 80 seats. "
        @"Setting up a coffee shop with 20 seats will cost you an additional $10,000. "
        @"Setting up a coffee shop with 80 seats will cost you more, an additional $40,000. "
        @"For every coffee sold, you make $3. "
        @"But apart from the setting up costs, the coffee shop incurs regular operational costs. "
        @"</p>"
        @"<h2>Setup the operations of the coffee shop</h2>"
        @"<p>"
        @"The coffee shop has fixed costs and variable costs to maintain its operations. "
        @"Every month, the cost of maintaining the coffee shop is deducted towards the end of the month. "
        @"</p>"
        @"<p>"
        @"Apart from monthly operational costs, the coffee shop requires labour. "
        @"Atleast one employee is required to serve coffee to customers. "
        @"The employees are paid on a weekly basis every Friday. "
        @"As a owner, you decide the number of employees required for your coffee shop. "
        @"</p>"
        @"<p>"
        @"A coffee shop requires coffee beans and milk to prepare coffee. "
        @"For making one coffee, 200 ml of milk and 100 grams of coffee bean is required. "
        @"Milk is a perishable item. Milk has to be procured every day. "
        @"Coffee bean is non-perishable item. It can be procured in bulk. "
        @"As a owner, you can decide the number of litres of milk that you want to order every day. "
        @"</p>"
        @"<h2>Monitor balance sheet and income statements</h2>"
        @"<p>"
        @"<strong>Balance sheet</strong> as the name implies is the balance money that you have. "
        @"The initial setup costs is deducted from the loan amount. "
        @"The rest of the balance is a simple difference between revenues and expenses. "
        @"</p>"
        @"<p>"
        @"<strong>Income statement</strong> is the profit made. "
        @"It is the difference between revenue and expenses. "
        @"</p>"
        @"</div>"
        @"</body>"
        @"</html>"
    };
    return html;
}

@end
