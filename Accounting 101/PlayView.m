//
//  PlayView.m
//  Accounting 101
//
//  Created by Vijay Thirugnanam on 14/01/14.
//  Copyright (c) 2014 Vijay Thirugnanam. All rights reserved.
//

#import "PlayView.h"

@interface PlayView()
@property (strong, nonatomic) UIActivityIndicatorView *activityIndicatorView;
@end

@implementation PlayView
@synthesize activityIndicatorView;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        activityIndicatorView = [[UIActivityIndicatorView alloc] initWithFrame:frame];
        activityIndicatorView.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
        activityIndicatorView.color = [UIColor colorWithRed:0 green:.3 blue:.9 alpha:1];
        [self addSubview:activityIndicatorView];
    }
    return self;
}


-(void)show
{
    [super show];
    [activityIndicatorView startAnimating];
}

-(void)hide
{
    [activityIndicatorView stopAnimating];
    [super hide];
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
