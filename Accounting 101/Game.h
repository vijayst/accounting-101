//
//  Game.h
//  Accounting 101
//
//  Created by Vijay Thirugnanam on 06/01/14.
//  Copyright (c) 2014 Vijay Thirugnanam. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AudioHelper.h"

@interface Game : NSObject
{
    int daysToPlay;
    int index;
}

@property int seats;
@property NSDate *startDate;
@property NSDate *currentDate;
@property int labor;
@property int milk;
@property float beans;
@property int coffee;
@property float balance;
@property float revenues;
@property float expenses;

@property (strong, nonatomic) NSMutableArray *messageArray;
@property (strong, nonatomic) AudioHelper *audioHelper;
@property (assign, nonatomic) id delegate;
@property NSInteger audioType;

+(void)reset;
+(Game *)getInstance;
+(Game *)getGameFromDb;
+(Game *)createGame:(int)pseats date:(NSDate *)pdate;
-(NSString *)getMessage;

-(void)playGame:(int)days audioType:(NSInteger)pAudioType interval:(float)pInterval;
-(void)playDay:(NSTimer *)timer;
@end

@protocol GamePlayedDelegate
@optional
-(void)gamePlayStarted;
-(void)gamePlayed:(Game *)game;
-(void)gamePlayEnded;
@end
