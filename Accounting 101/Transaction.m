//
//  Transaction.m
//  Accounting 101
//
//  Created by Vijay Thirugnanam on 06/01/14.
//  Copyright (c) 2014 Vijay Thirugnanam. All rights reserved.
//

#import "Transaction.h"
#import "DbHelper.h"

@interface Transaction()
-(float)add;
@property (strong, nonatomic) NSDateFormatter *formatter;
@end

@implementation Transaction

@synthesize num;
@synthesize debit;
@synthesize type;
@synthesize date;
@synthesize quantity;
@synthesize rate;
@synthesize formatter;

static int latest;

-(id)init
{
    self = [super init];
    if(self) {
        formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy-MM-dd"];
    }
    return self;
}

+(void)reset
{
    DbHelper *lhelper = [DbHelper getInstance];
    NSString *sql = [NSString stringWithFormat:@"DELETE FROM txn"];
    [lhelper executeCommand:sql];
}

+(Transaction *)getFirst
{
    DbHelper *lhelper = [DbHelper getInstance];
    NSString *sql = [NSString stringWithFormat:@"SELECT id, date([date]), debit, type, quantity FROM txn ORDER BY [date] ASC"];
    sqlite3_stmt *statement = nil;
    Transaction *trans = nil;
    if([lhelper executeQuery:sql statement:&statement])
    {
        if(sqlite3_step(statement) == SQLITE_ROW) {
            trans = [[Transaction alloc] init];
            NSString *formattedDate = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(statement, 1)];
            trans.date = [trans.formatter dateFromString:formattedDate];
            trans.num = sqlite3_column_int(statement, 0);
            trans.debit = (float)sqlite3_column_double(statement, 2);
            trans.type = sqlite3_column_int(statement, 3);
            trans.quantity = sqlite3_column_int(statement, 4);
            latest = trans.num;
        }
        sqlite3_finalize(statement);
    }
    return trans;
}

+(Transaction *)getLatest
{
    DbHelper *lhelper = [DbHelper getInstance];
    NSString *sql = [NSString stringWithFormat:@"SELECT id, date([date]), debit, type, quantity FROM txn ORDER BY [date] DESC"];
    sqlite3_stmt *statement = nil;
    Transaction *trans = nil;
    if([lhelper executeQuery:sql statement:&statement])
    {
        if(sqlite3_step(statement) == SQLITE_ROW) {
            trans = [[Transaction alloc] init];
            NSString *formattedDate = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(statement, 1)];
            trans.date = [trans.formatter dateFromString:formattedDate];
            trans.num = sqlite3_column_int(statement, 0);
            trans.debit = (float)sqlite3_column_double(statement, 2);
            trans.type = sqlite3_column_int(statement, 3);
            trans.quantity = sqlite3_column_int(statement, 4);
            latest = trans.num;
        }
        sqlite3_finalize(statement);
    }
    return trans;
}

-(float)getDebit:(int)ptype
{
    DbHelper *helper = [DbHelper getInstance];
    NSString *sql = [NSString stringWithFormat:@"SELECT SUM(debit) FROM txn WHERE type = %d", ptype];
    return [helper executeReal:sql];
}

-(float)getDebit:(int)ptype from:(NSDate *)from to:(NSDate *)to
{

    DbHelper *helper = [DbHelper getInstance];
    NSString *sql = [NSString stringWithFormat:@"SELECT SUM(debit) FROM txn WHERE type = %d AND date >= '%@' AND date < '%@'", ptype, [formatter stringFromDate:from], [formatter stringFromDate:to]];
    return [helper executeReal:sql];
}

-(int)getQuantity:(int)ptype
{
    DbHelper *helper = [DbHelper getInstance];
    NSString *sql = [NSString stringWithFormat:@"SELECT SUM(quantity) FROM txn WHERE type = %d", ptype];
    return (int)[helper executeReal:sql];
}

-(float)credit:(int)ptype
{
    return [self credit:ptype quantity:1 date:[NSDate date]];
}

-(float)debit:(int)ptype
{
    return [self debit:ptype quantity:1 date:[NSDate date]];
}

-(float)credit:(int)ptype quantity:(int)pquantity
{
    return [self credit:ptype quantity:pquantity date:[NSDate date]];
}

-(float)debit:(int)ptype quantity:(int)pquantity
{
    return [self debit:ptype quantity:pquantity date:[NSDate date]];
}

-(float)credit:(int)ptype date:(NSDate *)pdate
{
    return [self credit:ptype quantity:1 date:pdate];
}

-(float)debit:(int)ptype date:(NSDate *)pdate
{
    return [self debit:ptype quantity:1 date:pdate];
}

-(float)credit:(int)ptype quantity:(int)pquantity date:(NSDate *)pdate
{
    rate = -[self getRate:ptype];
    type = ptype;
    quantity = pquantity;
    date = pdate;
    return [self add];
}

-(float)debit:(int)ptype quantity:(int)pquantity date:(NSDate *)pdate
{
    rate = [self getRate:ptype];
    type = ptype;
    quantity = pquantity;
    date = pdate;
    return [self add];
}

-(float)getRate:(int)ptype
{
    DbHelper *helper = [DbHelper getInstance];
    NSString *sql = [NSString stringWithFormat:@"SELECT rate FROM txntype WHERE id = %d", ptype];
    return [helper executeReal:sql];
}

-(float)add
{
    debit = rate * quantity;
    if(latest==0)
        [Transaction getLatest];
    num = latest + 1;
    NSString *formattedDate = [formatter stringFromDate:date];
    NSString *sql = [NSString stringWithFormat:@"INSERT INTO txn (id, date, debit, type, quantity) VALUES (%d, '%@', %.1f, %d, %d)", num, formattedDate, debit, type, quantity];
    DbHelper *helper = [DbHelper getInstance];
    [helper executeCommand:sql];
    latest = num;
    return debit;
}

@end
