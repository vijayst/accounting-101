//
//  Game.m
//  Accounting 101
//
//  Created by Vijay Thirugnanam on 06/01/14.
//  Copyright (c) 2014 Vijay Thirugnanam. All rights reserved.
//

#import "Game.h"
#import "Shop.h"
#import "Transaction.h"
#import "BalanceSheet.h"
#import "DateHelper.h"
#import "CurrencyHelper.h"

@interface Game()
-(void)addMessage:(NSString *)message;
-(void)addHeader:(NSString *)header;
-(void)initGame;
-(void)initOperation;
@end

@implementation Game
@synthesize seats;
@synthesize startDate;
@synthesize currentDate;
@synthesize milk;
@synthesize labor;
@synthesize beans;
@synthesize coffee;
@synthesize balance;
@synthesize expenses;
@synthesize revenues;
@synthesize audioHelper;
@synthesize messageArray;
@synthesize delegate;
@synthesize audioType;

static Game *game;

+(Game *)getInstance
{
    return game;
}

-(id)init
{
    self = [super init];
    if(self) {
        self.milk = 10;
        self.labor = 1;
        self.beans = 100;
        self.coffee = 0;
        messageArray = [[NSMutableArray alloc] init];
        audioHelper = [[AudioHelper alloc] init];
        audioType = 1;
    }
    return self;
}

+(void)reset
{
    [Shop reset];
    [Transaction reset];
}

+(Game *)getGameFromDb
{
    Shop *shop = [Shop getLatest];
    if(shop) {
        game = [[Game alloc] init];
        if(game) {
            game.seats = shop.seats;
            game.labor = shop.labor;
            game.milk = shop.milk;
            Transaction *first = [Transaction getFirst];
            game.startDate = [first date];
            Transaction *last = [Transaction getLatest];
            game.currentDate = last.date;
            // get coffee and beans
            game.beans = 100;
            game.coffee = [last getQuantity:10];
            BalanceSheet * sheet = [BalanceSheet compute];
            game.balance = sheet.balance;
            game.revenues = sheet.sales;
            game.expenses = sheet.debit - sheet.setup;
            [game addMessage:@"Game restored. Continue playing!"];
        }
    }
    return game;
}

+(Game *)createGame:(int)pseats date:(NSDate*)pdate
{
    game = [[Game alloc] init];
    if(game) {
        game.seats = pseats;
        game.startDate = pdate;
        game.currentDate = pdate;
        [game initGame];
        [game initOperation];
    }
    return game;
}

-(void)initGame
{
    [self addHeader:@"New game"];
    Transaction *t = [[Transaction alloc] init];
    
    float debit = [t credit:1 date:currentDate];
    [self addMessage:[NSString stringWithFormat:@"Got a loan of %@.", [CurrencyHelper formatNumber:-debit]]];
    balance = -debit;
    debit = [t debit:2 date:currentDate];
    [self addMessage:[NSString stringWithFormat:@"Debited fixed costs of setting up shop: %@.", [CurrencyHelper formatNumber:debit]]];
    balance = balance - debit;
    debit = [t debit:3 quantity:seats date:currentDate];
    [self addMessage:[NSString stringWithFormat:@"Debited variable costs of setting up shop: %@ for %d seats.", [CurrencyHelper formatNumber:debit], seats]];
    balance = balance - debit;
    expenses = 0;
    revenues = 0;
}

-(void)initOperation
{
    labor = 1;
    milk = 10;
    beans = 100;
    coffee = 0;
}

-(void)playGame:(int)days audioType:(NSInteger)pAudioType interval:(float)pInterval
{
    audioType = pAudioType;
    [messageArray removeAllObjects];
    daysToPlay = days;
    float interval = pInterval;
    switch(days)
    {
        case 1:
            [audioHelper playDaySound:audioType];
            break;
        case 7:
            [audioHelper playWeekSound:audioType];
            break;
        case 30:
            [audioHelper playMonthSound:audioType];
            break;
    }
    if(delegate)
        [delegate gamePlayStarted];
    
    [NSTimer scheduledTimerWithTimeInterval:interval target:self selector:@selector(playDay:) userInfo:nil repeats:true];
}

-(void)playDay:(NSTimer *)timer
{
    [self addHeader:[DateHelper getString:currentDate format:@"d MMMM yyyy"]];
    NSDateComponents *components = [DateHelper getComponents:currentDate];
    Transaction *trans = [[Transaction alloc] init];
    float debit = 0;
    if(components.day==28) {
        debit = [trans debit:6 date:currentDate];
        [self addMessage:[NSString stringWithFormat:@"Debited fixed monthly expenses of %@.", [CurrencyHelper formatNumber:debit]]];
        balance -= debit;
        expenses += debit;
        debit = [trans debit:7 quantity:seats date:currentDate];
        [self addMessage:[NSString stringWithFormat:@"Debited variable monthly expenses of %@.", [CurrencyHelper formatNumber:debit]]];
        balance -= debit;
        expenses += debit;
    }
    if(components.weekday == 5) {
        debit = [trans debit:4 quantity:7*labor date:currentDate];
        [self addMessage:[NSString stringWithFormat:@"Debited weekly labour charges of %@.", [CurrencyHelper formatNumber:debit]]];
        balance -= debit;
        expenses += debit;
    }
    debit = [trans debit:8 quantity:milk date:currentDate];
    [self addMessage:[NSString stringWithFormat:@"Debited daily expenses for milk %@.", [CurrencyHelper formatNumber:debit]]];
    balance -= debit;
    expenses += debit;
    
    int maxCustomers = MIN(MIN(MIN(labor*100, milk*(5-(seats-20)*0.02)), seats*10), beans*10);
    if(labor < seats/10)
        maxCustomers = 0.6*maxCustomers;
    float f = 0.30 + 0.0001*pow(50-seats, 2);
    int minCustomers = floor(maxCustomers*f);
    int customers = minCustomers + arc4random() % (maxCustomers-minCustomers);
    
    debit = [trans credit:10 quantity:customers date:currentDate];
    [self addMessage:[NSString stringWithFormat:@"Credited sales of coffee: %@ for %d customers.", [CurrencyHelper formatNumber:-debit], customers]];
    balance -= debit;
    revenues -= debit;
    coffee += customers;
    
    beans-= customers/10;
    if(beans<=2)
    {
        beans = 1000;
        debit = [trans debit:9 quantity:100 date:currentDate];
        [self addMessage:[NSString stringWithFormat:@"Debited procurement expenses for 1000 kg of coffee beans: %@.", [CurrencyHelper formatNumber:debit]]];
        balance -= debit;
        expenses += debit;
    }
    
    currentDate = [currentDate dateByAddingTimeInterval:86400];
    
    if(delegate)
        [delegate gamePlayed:self];
    daysToPlay--;
    if(daysToPlay<=0) {
        [timer invalidate];
        timer=nil;
        if(delegate)
            [delegate gamePlayEnded];
    }
}

-(NSString *)getMessage
{
    return [messageArray componentsJoinedByString:@""];
}

-(void)addMessage:(NSString *)message
{
    [messageArray insertObject:[NSString stringWithFormat:@"<div>%@</div>", message] atIndex:index++];
}

-(void)addHeader:(NSString *)header
{
    index = 0;
    [messageArray insertObject:[NSString stringWithFormat:@"<h1>%@</h1>", header] atIndex:index++];
}

@end
