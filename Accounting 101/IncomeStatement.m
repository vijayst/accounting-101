//
//  IncomeStatement.m
//  Accounting 101
//
//  Created by Vijay Thirugnanam on 06/01/14.
//  Copyright (c) 2014 Vijay Thirugnanam. All rights reserved.
//

#import "IncomeStatement.h"
#import "Transaction.h"
#import "DateHelper.h"

@implementation IncomeStatement
@synthesize sales;
@synthesize revenues;
@synthesize operation;
@synthesize labor;
@synthesize milk;
@synthesize beans;
@synthesize expenses;
@synthesize profit;
@synthesize month;
@synthesize year;

+(IncomeStatement *)compute:(NSInteger)year month:(NSInteger)month
{
    IncomeStatement *income = [[IncomeStatement alloc] init];
    Transaction *trans = [[Transaction alloc] init];
    NSDate *from = [DateHelper getDate:1 month:month year:year];
    if(month==12) {
        month = 1;
        year++;
    } else month++;
    NSDate *to = [DateHelper getDate:1 month:month year:year];
    
    income.sales = -[trans getDebit:10 from:from to:to];
    income.revenues = income.sales;
    income.labor = [trans getDebit:4 from:from to:to] + [trans getDebit:5 from:from to:to];
    income.operation = [trans getDebit:6 from:from to:to] + [trans getDebit:7 from:from to:to];
    income.milk = [trans getDebit:8 from:from to:to];
    income.beans = [trans getDebit:9 from:from to:to];
    income.material = income.milk + income.beans;
    income.expenses = income.labor + income.material + income.operation;
    income.profit = income.revenues - income.expenses;
    return income;
}


@end
